package u05lab.code

object Exams {

  sealed trait ExamResultFactory {
    def failed(): ExamResult
    def retired(): ExamResult
    def succeededCumLaude(): ExamResult
    def succeeded(evaluation: Int): ExamResult
  }

  object ExamResultFactory {
    def apply(): ExamResultFactory = ExamResultFactoryImplementation()

    private case class ExamResultFactoryImplementation() extends ExamResultFactory {
      override def failed(): ExamResult = ExamResult(kind = Kind.FAILED)
      override def retired(): ExamResult = ExamResult(kind = Kind.RETIRED)
      override def succeededCumLaude(): ExamResult = ExamResult(cumLaude = true)
      override def succeeded(evaluation: Int): ExamResult = {
        if (evaluation < 18 || evaluation > 30) {
          throw new IllegalArgumentException
        }
        ExamResult(eval = evaluation)
      }
    }
  }

  sealed trait ExamResult {
    def getKind: Kind
    def getEvaluation: Option[Int]
    def cumLaude(): Boolean
  }

  object ExamResult {
    def apply(kind: Kind = Kind.SUCCEEDED, eval: Int = 18, cumLaude: Boolean = false): ExamResult = kind match {
      case Kind.FAILED => FailedExamResult()
      case Kind.RETIRED => RetiredExamResult()
      case Kind.SUCCEEDED if cumLaude => CumLaudeExamResult()
      case Kind.SUCCEEDED => SucceededExamResult(eval)
    }

    private case class FailedExamResult() extends AbstractExamResult(Kind.FAILED)
    private case class RetiredExamResult() extends AbstractExamResult(Kind.RETIRED)
    private case class CumLaudeExamResult() extends AbstractExamResult(Kind.SUCCEEDED) {
      override def getEvaluation: Option[Int] = Option(30)
      override def cumLaude(): Boolean = true
      override def toString: String = super.toString + "(30L)"
    }
    private case class SucceededExamResult(private val evaluation: Int) extends AbstractExamResult(Kind.SUCCEEDED) {
      override def getEvaluation: Option[Int] = Option(evaluation)
      override def toString: String = super.toString + s"($evaluation)"
    }
  }

  sealed trait Kind
  object Kind {
    case object FAILED extends Kind
    case object RETIRED extends Kind
    case object SUCCEEDED extends Kind
  }

  private abstract class AbstractExamResult(private val kind: Kind) extends ExamResult {
    override def getKind: Kind = kind
    override def getEvaluation: Option[Int] = Option.empty
    override def cumLaude(): Boolean = false
    override def toString: String = getKind.toString
  }
}

