package u05lab

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class SomeTest {

  import u05lab.code.List

  @Test
  def testIncremental() {
    val l = List("a", "b", "c")
    assertEquals(List.nil, List.nil.zipRight)
    assertEquals(List(("a", 0), ("b", 1), ("c", 2)), l.zipRight)
  }

  @Test
  def testPartition() {
    val l = List(0,1,2,3,4,5,6,7,8,9)
    val result = l.partition(_ > 5)
    assertEquals(List(6,7,8,9), result._1)
    assertEquals(List(0,1,2,3,4,5), result._2)
  }

  @Test
  def testSpan(){
    val l = List(0,1,2,3,4,5,6,7,8,9)
    val result = l.span(_ > 5)
    assertEquals(List.nil, result._1)
    assertEquals(l, result._2)
    val l2 = List(0,1,2,3,4,5,6,7,8,9)
    val result2 = l2.span(_ < 5)
    assertEquals(List(0,1,2,3,4), result2._1)
    assertEquals(List(5,6,7,8,9), result2._2)
  }

  @Test
  def testReduce() {
    val l = List(0,1,2,3,4,5,6,7,8,9)
    assertEquals(45, l.reduce(_+_))
    val l1 = List(0,1,2)
    assertEquals(0, l1.reduce(_*_))
    assertEquals(2, List(1,2).reduce(_*_))
  }

  @Test
  def testTakeRight() {
    val l = List(0,1,2,3,4,5,6,7,8,9)
    assertEquals(List(6,7,8,9), l.takeRight(4))
  }

  @Test
  def testCollect() {
    val l = List(0,5,10,20,40,80)
    assertEquals(List(0,5,10,40,80), l.collect { case x if x<15 || x>35 => x })
  }

  @Test
  def testSequence() {
    val list: List[Option[Int]] = List(Some(1), Some(2),Some(3))
    assertEquals(Some(List(1,2,3)), sequence(list))
    val list2: List[Option[Int]] = List(Some(1), None, Some(3))
    assertEquals(None, sequence(list2))
  }

  def sequence[A](list: List[Option[A]]): Option[List[A]] = list.foldRight(Option(List[A]()))((o1, o2) => o2 match {
    case Some(value) if o1.isDefined => Option(List.cons(o1.get, value))
    case _ => None
  })
}