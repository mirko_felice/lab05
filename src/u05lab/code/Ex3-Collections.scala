package u05lab.code

import java.util.concurrent.TimeUnit
import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.concurrent.duration.FiniteDuration

object PerformanceUtils {
  case class MeasurementResults[T](result: T, duration: FiniteDuration) extends Ordered[MeasurementResults[_]] {
    override def compare(that: MeasurementResults[_]): Int = duration.toNanos.compareTo(that.duration.toNanos)
  }

  def measure[T](msg: String)(expr: => T): MeasurementResults[T] = {
    val startTime = System.nanoTime()
    val res = expr
    val duration = FiniteDuration(System.nanoTime()-startTime, TimeUnit.NANOSECONDS)
    if(msg.nonEmpty) println(msg + " -- " + duration.toNanos + " nanos; " + duration.toMillis + "ms")
    MeasurementResults(res, duration)
  }

  def measure[T](expr: => T): MeasurementResults[T] = measure("")(expr)
}


object CollectionsTest extends App {
  val million = 1 to 1000000

  /* Linear sequences: List, ListBuffer */
  var list: scala.collection.immutable.List[Int] = _
  var listBuffer: ListBuffer[Int] = _

  /* Indexed sequences: Vector, Array, ArrayBuffer */
  var vector: Vector[Int] = _
  var array: Array[Int] = _
  var arrayBuffer: ArrayBuffer[Int] = _

  /* Sets */
  var set: Set[Int] = _
  var mutableSet: scala.collection.mutable.Set[Int] = _

  /* Maps */
  var map: Map[Int, Int] = _
  var mutableMap: mutable.Map[Int, Int] = _

  /* Comparison */
  import PerformanceUtils._
  val lst = (1 to 1000000).toList
  val vec = (1 to 1000000).toVector
  assert( measure("lst last"){ lst.last } > measure("vec last"){ vec.last } )

  println("\nCreate")
  measure("Immutable List create"){ list = scala.collection.immutable.List(million: _*) }
  measure("List Buffer create"){ listBuffer = ListBuffer(million: _*) }
  measure("Vector create"){ vector = Vector(million: _*) }
  measure("Array create"){ array = Array(million: _*) }
  measure("Array Buffer create"){ arrayBuffer = ArrayBuffer(million: _*) }
  measure("Immutable Set create"){ set = Set(million: _*) }
  measure("Mutable Set create"){ mutableSet = scala.collection.mutable.Set(million: _*)}
  measure("Immutable Map create"){ map = Map(million.zipWithIndex: _*) }
  measure("Mutable Map create"){ mutableMap = mutable.Map(million.zipWithIndex: _*) }

  println("\nRead Length/Size")
  measure("Immutable List length"){ list.length }
  measure("List Buffer length"){ listBuffer.length }
  measure("Vector length"){ vector.length }
  measure("Array length"){ array.length }
  measure("Array Buffer length"){ arrayBuffer.length }
  measure("Immutable Set size"){ set.size }
  measure("Mutable Set size"){ mutableSet.size }
  measure("Immutable Map size"){ map.size }
  measure("Mutable Map size"){ mutableMap.size }

  println("\nRead Last element")
  measure("Immutable List last"){ list.last }
  measure("List Buffer last"){ listBuffer.last }
  measure("Vector last"){ vector.last }
  measure("Array last"){ array.last }
  measure("Array Buffer last"){ arrayBuffer.last }
  measure("Immutable Set last"){ set.last }
  measure("Mutable Set last"){ mutableSet.last }
  measure("Immutable Map last"){ map.last }
  measure("Mutable Map last"){ mutableMap.last }

  println("\nUpdate")
  measure("Immutable List append"){ list.appended(-1) }
  measure("List Buffer append"){ listBuffer.addOne(-1) }
  measure("Vector append"){ vector.appended(-1) }
  measure("Array append"){ array.appended(-1) }
  measure("Array Buffer append"){ arrayBuffer.appended(-1) }
  measure("Immutable Set append"){ set + (-1) }
  measure("Mutable Set append"){ mutableSet + (-1) }
  measure("Immutable Map append"){ map + (-1 -> -1) }
  measure("Mutable Map append"){ mutableMap + (-1 -> -1) }

  println("\nDelete")
  measure("Immutable List delete"){ list.dropRight(1) }
  measure("List Buffer delete"){ listBuffer.remove(listBuffer.size - 1) }
  measure("Vector delete"){ vector.dropRight(1) }
  measure("Array delete"){ array.dropRight(1) }
  measure("Array Buffer delete"){ arrayBuffer.remove(arrayBuffer.size - 1) }
  measure("Immutable Set delete"){ set - (-1) }
  measure("Mutable Set delete"){ mutableSet - (-1) }
  measure("Immutable Map delete"){ map - (-1) }
  measure("Mutable Map delete"){ mutableMap - (-1) }
}