package u05lab.code

import u05lab.code.Exams.ExamResult

import scala.collection.mutable.{Map => MutableMap}

sealed trait ExamsManager {

  def createNewCall(call: String): Unit

  def addStudentResult(call: String, student: String, result: ExamResult): Unit

  def getAllStudentsFromCall(call: String): Set[String]

  def getEvaluationsMapFromCall(call: String): Map[String, Int]

  def getResultsMapFromStudent(student: String): Map[String, String]

  def getBestResultFromStudent(student: String): Option[Int]
}

object ExamsManager {
  def apply(): ExamsManager = ExamsManagerImplementation()

  private case class ExamsManagerImplementation() extends ExamsManager {

    private var callsMap: Map[String, MutableMap[String, ExamResult]] = Map()

    override def createNewCall(call: String): Unit = {
      checkArgument(!callsMap.contains(call))
      callsMap += call -> MutableMap()
    }

    override def addStudentResult(call: String, student: String, result: ExamResult): Unit = {
      checkArgument(callsMap.contains(call))
      checkArgument(!callsMap(call).contains(student))
      callsMap(call) += (student -> result)
    }

    override def getAllStudentsFromCall(call: String): Set[String] = {
      checkArgument(callsMap.contains(call))
      Set.from(callsMap(call).keys)
    }

    override def getEvaluationsMapFromCall(call: String): Map[String, Int] = {
      checkArgument(callsMap.contains(call))
      val f :((String, ExamResult)) => (String, Int) = (v) => (v._1, v._2.getEvaluation.get)
      var result: Map[String, Int] = Map()
      callsMap(call).view.foreachEntry((k,v) => {
        if (v.getEvaluation.isDefined)
          result += k -> v.getEvaluation.get
      })
      result
    }

    override def getResultsMapFromStudent(student: String): Map[String, String] = {
      var result: Map[String, String] = Map()
      callsMap.foreachEntry((k,v) => {
        if (v.contains(student))
          result += k -> v(student).toString
      })
      result
    }

    override def getBestResultFromStudent(student: String): Option[Int] = {
      callsMap.values
        .collect {case map if map.contains(student) && map(student).getEvaluation.isDefined => map(student).getEvaluation.get}
        .maxOption((x: Int,y: Int) => x-y)
    }

    private def checkArgument(condition: Boolean): Unit = {
      if (!condition)
        throw new IllegalArgumentException
    }
  }
}
